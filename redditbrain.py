from praw import Reddit
from praw import exceptions as praw_exceptions
from prawcore import exceptions as prawcore_exceptions
from requests import exceptions as requests_exceptions
from click import echo, command, option, group
from asciistuff import Lolcat
from datetime import datetime
from countdown import countdown
from yaml import load, FullLoader # using yaml because json for some reason can't support single quotes and comments
from random import choice, randint
from settings import error

from settings.actions import post_handler, comment_handler, detect_shadow_ban 
from settings.strings import strftime
from settings.cache import set_cache, get_cache, set_config, get_config, flush_cache
from settings.stages import Stages

def internal(stage, reddit, post_cache, comment_cache, config_file, conf, karma, my_info, username):
    if stage == 1:
        stage_content = Stages.stage1
    elif stage == 2:
        stage_content = Stages.stage2
    elif stage == 3:
        stage_content = Stages.stage3
    elif stage == 4:
        stage_content = Stages.stage4
    else:
        error(f"Invalid stage passed.", "red")

    if "onlyNSFW" in conf and conf["onlyNSFW"] == True:
        subreddits = []
    else:
        subreddits = stage_content.subreddits

    if my_info.is_suspended == True or detect_shadow_ban(username) == True:
        error(f"You are suspended from Reddit.", "red")

    if "nsfw" in conf and conf["nsfw"] == "true":
        subreddits += stage_content.nsfw_subreddits 

    for i in range(stage_content.loop):
        if stage == 1 and karma <= 100:
            error("Can only post to a few subreddits since your karma is so low", "yellow")
            post_subreddit = reddit.subreddit(choice(["help", "FreeKarma4U"]))
        
        else:
            post_subreddit = reddit.subreddit(choice(subreddits))

        post_result = post_handler(reddit, post_subreddit, karma)

        if stage == 1 and karma <= 100:
            error("Flushing selfpost cache due to low karma", "blue")
            flush_cache(False)

        if post_result == "valid":
            error(f"Submitted a post to {post_subreddit}!", "green")
            break
        
        elif post_result == "invalid":
            error(f"The subreddit {post_subreddit} has no action associated with it. Configure an action in ./settings/actions.py", "yellow")

        elif post_result == "skipped":
            error(f"Posting to {post_subreddit} was skipped because it hasn't been more than 3 days since you posted there.", "yellow")

        elif post_result == "skipped:karma":
            error(f"Posting to {post_subreddit} was skipped because you don't have enough karma.", "yellow")

    for i in range(stage_content.comment_loop):
        countdown(mins=1, secs=randint(0, 60))
        comment_handler(reddit, stage, subreddits)

    error(f"SNOOsing...", "blue")
    

    if karma <= 100:
        error(f"Running longer cooldown to prevent shadow bans.", "blue")
        countdown(mins=randint(20, 30), secs=randint(0, 60))

    else:
        countdown(mins=randint(stage_content.min_cooldown, stage_content.max_cooldown), secs=randint(0, 60))

@group()
def main():
    """
A karma farming bot for Reddit made with PRAW and Click. It's designed to be incredibly efficient, and set-and-forget taking into account anything that could cause a ban and all the karma rules of various subreddits. It also detects account suspensions and shadow bans in case one occurs.
    """
    pass

@command()
@option("-i", "--client-id", help="Set the Reddit Client ID.", required=True)
@option("-c", "--client-password", help="Set the Reddit Client Password.", required=True)
@option("-u", "--username", help="Set the username.", required=True)
@option("-p", "--password", help="Set the Password.", required=True)
def run(client_id, client_password, username, password):
    """
Actually run the bot
    """

    echo(Lolcat("""
       __
   _  |@@|
  / \ \--/ __
  ) O|----|  |   __
 / / \ }{ /\ )_ / _|
 )/  /\__/\ \__O (__
|/  (--/\--)    \__/
/   _)(  )(_
   `---''---`

<- Reddit-Brain-v2 is running. Try to catch its bugs! ->
    """))
    reddit = Reddit(client_id=client_id, client_secret=client_password, username=username, password=password, user_agent="this is simply for using on reddit")
    primary_loop_count = 0
    while 1==1:
        with open("cache/post.yml", "r+") as post_cache:
            with open("cache/comment.yml", "r+") as comment_cache:
                with open("cache/config.yml", "r+") as config_file:
                    try:
                        if primary_loop_count == 100:
                            error(f"Reached 100 loops. Flushing the cache...", "blue")
                            
                            flush_cache(False)

                        my_info = reddit.redditor(username)
                        karma = my_info.total_karma 
                        comment_karma = my_info.comment_karma
                        conf = load(config_file, Loader=FullLoader) 

                        if my_info.is_suspended == True or detect_shadow_ban(username) == True:
                            error(f"You are suspended from Reddit.", "red")

                        if karma >= Stages.stage1.karma and karma < Stages.stage2.karma:
                            error(f"{datetime.now().strftime(strftime())} Your account has {karma} karma. Running in stage 1.", "blue")
                            internal(1, reddit, post_cache, comment_cache, config_file, conf, karma, my_info, username)
                        
                        elif karma >= Stages.stage2.karma and karma < Stages.stage3.karma:
                            error(f"Your account has {karma} karma. Running in stage 2.", "blue")
                            internal(2, reddit, post_cache, comment_cache, config_file, conf, karma, my_info, username)

                        elif karma >= Stages.stage3.karma and karma < Stages.stage4.karma:
                            error(f"Your account has {karma} karma. Running in stage 3.", "blue")
                            internal(3, reddit, post_cache, comment_cache, config_file, conf, karma, my_info, username)

                        elif karma >= Stages.stage4.karma:
                            error(f"Your account has {karma} karma. Running in stage 4.", "blue")
                            internal(4, reddit, post_cache, comment_cache, config_file, conf, karma, my_info, username)

                        primary_loop_count += 1

                    except praw_exceptions.WebSocketException:
                        error(f"Websocket Malfunctioned Error: A server issue with Reddit.", "yellow")
                    except praw_exceptions.RedditAPIException:
                        error(f"Thread Locked / Ratelimit Error: This happens if your account is new, you were suspended from a subreddit or a thread was locked by a moderator.", "yellow")
                    except praw_exceptions.TooLargeMediaException:
                        error(f"Image too large Error: The image is too large to submit.", "yellow")
                    except prawcore_exceptions.Forbidden:
                        error(f"Forbidden Error: Probably because the thread was locked or bad API call.", "yellow")
                    except requests_exceptions.ConnectionError or prawcore.exceptions.ServerError:
                        error(f"Network connection Error: Probably an issue with reddit or an external API.", "yellow")
                    except prawcore_exceptions.ResponseException:
                        error(f"Reddit is down right now, fix your servers, asshole.", "yellow")
                        countdown(mins=1)
                    except KeyError:
                        error(f"Failed to download gallery. You are likely banned from this subreddit.", "yellow")
                        countdown(mins=1)

@command()
@option("-f", "--flush-the-cache", help="Flush the post, comment and settings cache. This is basically a factory reset.", is_flag=True)
@option("-n", "--allow-nsfw", help="Allow posting to NSFW subreddits.", is_flag=True)
@option("-N", "--deny-nsfw", help="Deny posting to NSFW subreddits.", is_flag=True)
@option("-s", "--allow-force-stage", help="Force running in a specific stage.", type=int)
@option("-S", "--deny-force-stage", help="Deny running in a specific stage and just do it based on karma.", is_flag=True)
@option("-o", "--allow-only-nsfw", help="Only post to NSFW subreddits.", is_flag=True)
@option("-O", "--deny-only-nsfw", help="Deny only posting to NSFW subreddits,", is_flag=True)
def settings(flush_the_cache, allow_nsfw, deny_nsfw, allow_force_stage, deny_force_stage, allow_only_nsfw, deny_only_nsfw):
    """
Settings for the bot.
    """
    if flush_the_cache:
        answer = input("Are you sure? This will erase all config [Y/n]: ")

        if answer in ["Y", "y"]:
            flush_cache(True)
            error(f"Erased the cache successfully. ", "green")
        elif answer in ["N", "n"]:
            error("Alright, won't erase cache.", "blue")
        else:
            error("Invalid answer.", "red")

    elif allow_nsfw == True and deny_nsfw == True:
        error(f"You can't do both at the same time", "red")

    elif allow_nsfw == True:
        set_config("cache/config.yml", "nsfw", "true")
        error(f"Allowed NSFW subreddits.", "green")
    
    elif deny_nsfw == True:
        set_config("cache/config.yml", "nsfw", "false")
        error(f"Denied NSFW subreddits.", "green")

    elif allow_force_stage != None:
        if allow_force_stage > 0 and allow_force_stage <= 3: 
            set_config("cache/config.yml", "forceStage", str(allow_force_stage))
            error(f"Set forcestage to {allow_force_stage}.", "green")
            exit()
        else:
            error("Invalid stage. Must be 1, 2, or 3.", "red")
    
    elif deny_force_stage == True:
        set_config("cache/config.yml", "forceStage", "false")
        error(f"No longer forcing a stage.", "green")

    elif allow_only_nsfw == True:
        set_config("cache/config.yml", "onlyNSFW", "true")
        error(f"Will only post NSFW from now on.", "green")

    elif deny_only_nsfw == True:
        set_config("cache/config.yml", "onlyNSFW", "false")
        error(f"Denying NSFW from now on.", "green")

    else:
        error("An invalid option was sent.", "red")

if __name__ == "__main__":
    main.add_command(run)
    main.add_command(settings)
    main()
