class Stage:
    def __init__(self, **kwargs):
        self.karma = kwargs.get("karma")
        self.loop = kwargs.get("loop")
        self.min_cooldown = kwargs.get("min_cooldown")
        self.max_cooldown = kwargs.get("max_cooldown")
        self.subreddits = kwargs.get("subreddits") 
        self.nsfw_subreddits = kwargs.get("nsfw_subreddits")
        self.comment_loop = kwargs.get("comment_loop")

class Stages:
    global_commentables = ["worldnews", "AmITheAsshole", "MadeMeSmile", "Catswithjobs", "confusing_perspective", "FreeCompliments", "gaming", "houseplants", "me_irl", "technicallythetruth", "todayilearned", "mildlyinteresting", "MadeMeSmile", "Madlads", "worldnews"]

    stage1 = Stage(
        karma=0, 
        loop=1, 
        min_cooldown=30, 
        max_cooldown=40,
        subreddits=["antiwork", "NewToReddit", "collapse", "linuxmemes", "mildlyinfuriating", "hmmm", "cursedcomments", "cursedcursedcomments", "pics"],
        nsfw_subreddits=["yiff", "FreeKarma4U"],
        comment_loop=1,
    )

    stage2 = Stage(
        karma=500,
        loop=2,
        min_cooldown=25,
        max_cooldown=40,
        subreddits=stage1.subreddits + ["itookapicture", "Memes_Of_The_Dank", "me_irl", "cats", "offmychest", "AskReddit", "196", "197", "198", "shitposting", "animemes", "aww", "dankmemes"], 
        nsfw_subreddits=stage1.nsfw_subreddits + ["slutwife", "Overwatch_Porn", "blowjobs", "asshole", "GodAsshole", "sluttyconfessions", "RateMyPussy", "xsmallgirls", "CedehsHentai", "Lewdarium"],
        comment_loop=2,
    )

    stage3 = Stage(
        karma=20000,
        loop=2,
        min_cooldown=23,
        max_cooldown=40,
        subreddits=stage2.subreddits + ["memes", "meme", "awww"],
        nsfw_subreddits=stage2.nsfw_subreddits + ["gonewild30plus", "bigasses", "amateur_milfs"],
        comment_loop=3,
    )

    # if your account has more than 50000 karma. 
    stage4 = Stage(
        karma=50000,
        loop=2,
        min_cooldown=22,
        max_cooldown=35,
        subreddits=stage2.subreddits + ["crappydesign", "linuxmasterrace", "4chan", "LateStageCapitalism", "Animemes"],
        nsfw_subreddits=stage2.nsfw_subreddits + ["AnalGW", "anal", "blowjob", "adorableporn", "yiffminus", "BreedingMaterial", "altgonewild", "GodPussy", "LipsThatGrip", "pussy", "hentai"],
        comment_loop=3
    )

