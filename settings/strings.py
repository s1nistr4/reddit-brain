from random import choice
def strftime():
    return "%b %e, %Y %H:%M:%S %p"

class Definers:
    meme = ["meme", "meem", "maymay", "may may", "memeable content"]
    memes = ["memes", "meems", "maymays", "may mays"]
    aww_or_cats = ["cat", "kitten", "kitty cat", "cat", "kitty", "floof", "neko", "average floof"]
    post = ["post", "submission", "content"]
    congratulate = ["nice", "incredible", "epic", "cool", "dope", "chad", "wonderful"]
    person = ["homie", "dude", "bro", "brochacho", "kind sir", "my dude"]
    humor = ["xd", "oof", "lol"]
    quality = ["low quality", "high quality", "moderate quality", "somewhat high quality", "questionably high quality", "barely a high quality"]
    nsfw = ["porn" "nsfw", "sexy content", "adult content", "pornographic content", "nsfw content"]
    # emotes
    happy_emote = [":)", ":0", ":]", ";)", ";]"]    
    sad_emote = [":)", ":0", ":]", ";)", ";]"]
    surprised_emote = [":)", ":0", ":]", ";)", ";]"]
    all_emotes = happy_emote + sad_emote + surprised_emote

class Titles:
    meme = [
        f"Please appreciate my {choice(Definers.meme)}", 
        f"Appreciate my {choice(Definers.meme)}", 
        f"God my {choice(Definers.memes)} suck", 
        f"I created this {choice(Definers.meme)} from my sex dungeon", 
        f"Just your average {choice(Definers.meme)}", 
        f"{choice(Definers.meme)}", 
        f"Very low quality {choice(Definers.meme)}", 
        f"lol {choice(Definers.congratulate)}", 
        f"le {choice(Definers.congratulate)}", 
        f"literally {choice(Definers.congratulate)}", 
        f"literally not {choice(Definers.congratulate)}",
        f"not {choice(Definers.congratulate)}",
        f"{choice(Definers.congratulate)}", 
        f"{choice(Definers.all_emotes)}",
        f"{choice(Definers.humor)}",
        "i want to die", 
        "i wants to die",
        "I want 2 die", 
        "Just another random post", 
        "Add some sort of title here", 
        "hecking frick", 
        "Title goes here", 
        "Totally Relatable", 
        "This is relatable I guess", 
        "This will die in new", 
        "End my pain", 
        "Sideways text", 
        "I am once again asking for your upvotes", 
        "took a while for some reason lol", 
        "Placeholder text", 
        "My mom checks my phone", 
        "The pain never stops", 
        "cha cha real smooth", 
        "it be like that tho", 
        "it really be like that", 
        "I'm too lazy to add a proper title", 
        "upper text", 
        "you may laugh now", 
        "idk what to put here", 
        "Bottom Text", 
        "AAAAAAAAAAAAA", 
        "It truly do be like that", 
        "Took me a while like fr plz send help",
    ]

    greentext = [
        f"anon is {choice(Definers.congratulate)}",
        "real and straight",
        "fake and homosexual",
    ]

    porn = [
        f"some {choice(Definers.nsfw)}",
        f"yet some more {choice(Definers.nsfw)}",
        f"even more {choice(Definers.nsfw)}",
        f"{choice(Definers.nsfw)}",
        f"{choice(Definers.nsfw)} content",
        f"just some more {choice(Definers.nsfw)} content", 
        f"low effort {choice(Definers.nsfw)} content",
        f"just some more {choice(Definers.nsfw)}",
        "this will die in new",
        "hot"
    ]

    aww_or_cats = [
        f"Cute {choice(Definers.aww_or_cats)}",
        f"Just a {choice(Definers.aww_or_cats)}",
        f"Just another {choice(Definers.aww_or_cats)}", 
        f"Once again another {choice(Definers.aww_or_cats)}",
        f"Here's a {choice(Definers.aww_or_cats)} I guess", 
        f"Just your average {choice(Definers.aww_or_cats)}", 
        f"here's a {choice(Definers.aww_or_cats)}",
        f"Random {choice(Definers.aww_or_cats)} pic I found",      
        f"Floof {choice(Definers.aww_or_cats)}", 
        f"literally just a {choice(Definers.aww_or_cats)}",
        f"Here's a cute {choice(Definers.aww_or_cats)}", 
        f"What a cute {choice(Definers.aww_or_cats)}", 
        "Meow", 
        f"Kitty {choice(Definers.aww_or_cats)}", 
        "M E O W", 
        f"Floof {choice(Definers.aww_or_cats)}", 
        "Here's a random pic I found", 
        ":0", 
        "Add Title Here", 
        "The floof", 
    ]

    animemes = meme + [
        f"Just your typical anime {choice(Definers.meme)}",
    ]
