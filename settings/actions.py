from __future__ import annotations
from random import choice
from filetype import guess
from datetime import datetime
from dateutil import parser
from requests import get as rget
import os

from settings import error
from .strings import Titles
from .cache import set_cache, get_cache, set_config, get_config, flush_cache
from .stages import Stages

# ik this is hacky but it works and couldn't find another way to do it without delving into reddit api hell.
# i swear new reddit opts in for "security by spamming as many request/response headers as possible"
def detect_shadow_ban(username):
    headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0"}
    r = rget(f"https://old.reddit.com/user/{username}", headers=headers)

    if r.status_code == 404:
        return True
    else:
        return False

def post_handler(reddit, post_subreddit, karma):
    def cross_post(src, reddit, post_subreddit, **kwargs):
        sub = reddit.subreddit(src)
        
        while 1==1:
            post = choice(list(sub.hot(limit=100)))
            
            if get_cache("cache/post.yml", content_id=post.id) == False:
                break
            else:
                error(f"The post \"{post.title}\" has already been reposted. Trying another one...", "yellow")
                

        if kwargs.get("title") != None:
            post_title = kwargs.get("title")
   
        elif post_subreddit == "itookapicture":
            post_title = f"ITAP of {post.title}" 
        
        elif post_subreddit == "smallboobs":
            while 1==1:
                post = choice(list(sub.hot(limit=100)))

                if "redgifs.com" not in post.url and "imgur.com" not in post.url and "redd.it" not in post.url and "reddit.com" not in post.url:
                    break
                else:
                    post_title = post.title
                    break          

        elif post_subreddit == "meme":
            if "upvote" in post.title:
                title = post.title
                title.replace("upvote", "")
                post_title = title
            else:
                post_title = post.title
   
        elif post_subreddit == "blowjobs":
            while 1==1:
                post = choice(list(sub.hot(limit=100)))

                if "redgifs.com" not in post.url:
                    break
                else:
                    post_title = post.title
                    break
                
        else:
            post_title = post.title
        
        if post.selftext not in [None, ""]:
            post_subreddit.submit(title=post_title, selftext=post.selftext)

        elif post.url not in ["", None]:
            # if praw doesn't let you steal galleries then fuck it, we gonna' do this like we do downeer in tennisseee or some shit
            # this is probably illegal in several countries and planets.
            if "https://www.reddit.com/gallery/" in post.url:
                url_reddit_template = f"https://www.reddit.com/r/{post.subreddit}/comments/{post.id}.json"
                headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0"}
                req = rget(url_reddit_template, headers=headers).json()[0]['data']
                gallery_urls = []

                for i in range(len(req['children'][0]['data']["media_metadata"])):
                    gallery_id = req['children'][0]['data']['gallery_data']['items'][i]['media_id']
                    url = req['children'][0]['data']['media_metadata'][gallery_id]['s']['u']
                    gallery_urls += [url.replace("amp;", "")]

                loop = 0
                paths = []

                for i in gallery_urls:
                    req2 = rget(i, headers=headers, stream=True).content
                    ft = guess(req2).extension
                
                    with open(f"cache/gallery{loop}.{ft}", "wb") as f:
                        f.write(req2)
                        paths += [f"cache/gallery{loop}.{ft}"]
                        loop += 1

                uploaded_images = []
                
                for i in paths:
                    uploaded_images += [{"image_path": i}]

                post_subreddit.submit_gallery(title=post_title, images=uploaded_images)
                
                loop2 = 0
                for i in paths:
                    os.remove(f"cache/gallery{loop2}.{ft}")
                    loop2 += 1
                
                loop = 0
                # how this actually worked is beyond my comprehension
            else:
                post_subreddit.submit(title=post_title, url=str(post.url))
                
        else:
            post_subreddit.submit(title=post_title, selftext="")
        
        return "act_completed"

    def self_post(post_subreddit):
        cache = get_cache("cache/self_post.yml", subreddit=post_subreddit)

        if cache != False:
            current_time = datetime.now()
            timestamp = parser.parse(cache["timestamp"])
            time_passed = current_time - timestamp

            if time_passed.days < 3:
                return "skipped"

        while 1==1:
            post = choice(list(post_subreddit.hot(limit=100)))
            
            if get_cache("cache/post.yml", content_id=post.id) == False:
                break
            
            else:
                error("The post {post.title} has already been reposted. Trying another one...", "blue")
                

        if post.selftext not in ["", None]:
            post_subreddit.submit(title=post.title, selftext=post.selftext)
        

        elif post.url not in ["", None] and "https://www.reddit.com/" not in post.url:
            post_subreddit.submit(title=post.title, url=post.url)
        
        else:
            post_subreddit.submit(title=post.title, selftext="")

        set_cache("cache/self_post.yml", post_subreddit, datetime.now())
        set_cache("cache/post.yml", post_subreddit, datetime.now(), content_id=post.id)
        
        return "act_completed"

    error(f"Posting to {post_subreddit}", "blue")

    if post_subreddit in ["Memes_Of_The_Dank", "me_irl", "meme", "memes", "dankmemes", "196", "197", "198", "shitposting"]:
        subs = ["meme", "memes", "Memes_Of_The_Dank", "dankmemes", "shitposting", "me_irl", "196", "197", "198"]

        if post_subreddit == "dankmemes" and karma <= 5000:
            return "skipped:karma"

        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit, title=choice(Titles.meme))

    elif post_subreddit in ["cats", "awww", "aww"]:
        req = rget("https://api.thecatapi.com/v1/images/search")
        post_subreddit.submit(title=choice(Titles.aww_or_cats), url=req.json()[0]["url"])
        res = "act_completed"

    elif post_subreddit == "collapse":
        src = choice(["ABoringDystopia", "LateStageCapitalism"])
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit == "politicalhumor":
        subs = ["PoliticalCompassMemes"]
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["offmychest", "rant"]:
        subs = ["offmychest", "rant"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    # self post subreddits
    elif post_subreddit in ["help", "NewToReddit", "AskReddit", "AdviceAnimals", "whatisthisthing", "FreeKarma4You"]:
        res = self_post(post_subreddit)
    
    elif post_subreddit in ["pics", "itookapicture"]:
        subs = ["pics", "itookapicture"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit == "linuxmemes":
        src = choice(["linuxmasterrace", "linuxcirclejerk"])
        res = cross_post(src, reddit, post_subreddit)
   
    elif post_subreddit == "antiwork":
        src = "recruitinghell"
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit == "mildlyinfuriating":
        src = "crappydesign"
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit == "hmmm":
        src = choice(["blursedimages", "cursedimages"])
        res = cross_post(src, reddit, post_subreddit, title="hmmm")

    elif post_subreddit in ["cursedcomments", "cursedcursedcomments"]:
        subs = ["cursedcomments", "cursedcursedcomments"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit == "Whatisthis":
        subs = ["whatisthisthing", "Whatisthis"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["greentext", "4chan"]:
        subs = ["greentext", "4chan"]
        subs.remove(post_subreddit)
        src = choice(subs) 

        if src == "4chan":
            res = cross_post(src, reddit, post_subreddit)
        
        elif src == "greentext":
            res = cross_post(src, reddit, post_subreddit, title=choice(Titles.greentext))

    elif post_subreddit == "Animemes":
        subs = ["Animemes", "goodanimemes", "okbuddybaka", "animememes", "wholesomeanimemes"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit, title=choice(Titles.animemes))

    # NSFW
    elif post_subreddit in ["adorableporn", "LegalTeens", "xsmallgirls"]:
        src = choice(["Tita_Sahara", "AVfanatics", "Aka_Asuka", "Tia_Ling", "ConniePerignon_", "collegesluts", "xsmallgirls", "LegalTeens"])
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["yiff", "yiffminus"]:
        subs = ["yiffminus", "yiff"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)
   
    elif post_subreddit in ["blowjobs", "blowjob"]:
        subs = ["blowjobs", "blowjob"]
        subs.remove(post_subreddit)
        src = choice(subs)

        res = cross_post(src, reddit, post_subreddit)
    
    elif post_subreddit in ["asshole", "anal", "GodAsshole", "AnalGW", "bigasses"]:
        subs = ["anal", "asshole", "ass", "booty_queens", "asstastic", "GodAsshole", "AnalGW", "bigasses"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["Overwatch_Porn"]:
        subs = ["widowmakerNSFW", "TracerNSFW", "MeiNSFW", "OverwatchFutaFun", "Overwatch_Porn"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["GodPussy", "LipsThatGrip", "PussyFlashing", "RateMyPussy", "pussy", "pussyrating"]:
        subs = ["GodPussy", "LipsThatGrip", "PussyFlashing", "RateMyPussy", "pussy", "pussyrating"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["AdorableNudes", "DaughterTraining", "HappyEmbarassedGirls", "xsmallgirls"]:
        subs = ["AdorableNudes", "CollegeAmateurs", "DadWouldBeProud", "DaughterTraining", "HappyEmbarassedGirls", "LegalTeens", "SmallCutie", "collegesluts", "funsized", "smallboobs", "xsmallgirls"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)
    
    elif post_subreddit in ["slutwife", "amateur_milfs", "gonewild30plus"]:
        subs = ["milf", "slutwife", "maturemilf", "gonewild30plus", "amateur_milfs"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["BreedingMaterial", "altgonewild", "needysluts", "nsfw", "FreeKarma4U"]:
        subs = ["BreedingMaterial", "slut", "blowjobs", "blowjob", "AllDayFuckNSFW", "HotFreebies", "ReStrictD", "RealGirls", "SheLikesItRough", "altgonewild", "gonewild", "FreeKarma4U", "needysluts", "nsfw", "nudes"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit == "sluttyconfessions":
        subs = ["sluttyconfessions", "gonewildstories"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)

    elif post_subreddit in ["CedehsHentai", "Lewdarium", "hentai", "hentai_gifs"]:
        subs = ["CedehsHentai", "Lewdarium", "Rule34", "hentai", "hentai_gifs", "hentaifemdom", "saohentai"]
        subs.remove(post_subreddit)
        src = choice(subs)
        res = cross_post(src, reddit, post_subreddit)
   
    else:
        error(f"No subreddit was sent to actions.py {post_subreddit.display_name}!", "yellow")
        return "invalid"

    if not res:
        error(f"The logic for {post_subreddit.display_name} hasn't been programmed yet.", "red")

    elif res == "act_completed":
        return "valid"
    elif res == "skipped":
        return "skipped"
    else:
        return "invalid"

def comment_handler(reddit, stage, subreddits):
    while 1==1:
        while 1==1:
            comment_sub = reddit.subreddit(choice(Stages.global_commentables + subreddits)) # get a random subreddit
            post = choice(list(comment_sub.hot(limit=10))) # get a random post from hot in that subreddit in hot
            comments = post.comments # get all the comments on that post

            if len(list(comments)) <= 20:
                error(f"Too few comments on this post, trying another...", "yellow")
            else:
                post_id = str(post.id) # get post id
                post.comment_sort = "top"
                loop = 0
                break

        for comment in list(comments): # for each comment
            if loop == 2:
                error("Couldn't find a comment with under 3 replies. We'll try this again with a different post...", "yellow")
                break
            else:
                if comment.author not in ["AutoModerator"] and len(list(comment.replies)) <= 2:
                    post2 = reddit.submission(post_id)
                    post2.comment_sort = "new"
                    post_comments_new = post2.comments
                    comment_body = list(post_comments_new)
                    
                    if "I am a bot" not in comment_body[0].body and "/u/redditsave" not in comment_body[0].body and "Support this service on" not in comment_body[0].body:
                        if get_cache("cache/comment.yml", content_id=comment.id) == False:
                            post2 = reddit.submission(post_id)
                            post2.comment_sort = "new"
                            res = comment.reply(comment_body[0].body)
                            set_cache("cache/comment.yml", None, None, content_id=comment.id)

                            error(f"Added a comment to post {post.url}", "green")
                            return "valid"
                        
                        else:
                            error(f"Already commented here. Trying another location...", "yellow")
                            break
                    else:
                        error(f"This comment is from a bot. Let's try another comment...", "yellow")
                        break

                else:
                    loop += 1
