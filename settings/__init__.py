from datetime import datetime
from colorama import Fore, Style
from .strings import strftime
from click import echo

def error(message, color):
    if color == "red":
        color = Fore.RED
    elif color == "green":
        color = Fore.GREEN
    elif color == "blue":
        color = Fore.BLUE
    elif color =="yellow":
        color = Fore.YELLOW
    else:
        echo("An invalid color parameter was passed to the error() function.")
        exit()
    
    print(f"{color}{datetime.now().strftime(strftime())} {message}")
    echo(Style.RESET_ALL)

    if color == "red":
        exit()
