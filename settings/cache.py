from yaml import load, FullLoader
from colorama import Fore, Style
import os

from .strings import Titles
from settings import error

def set_config(filename, key, value):
    if os.path.exists(filename):
        with open(filename, "r") as f:
            config = load(f, Loader=FullLoader)
            config[key] = value

            with open(filename, "w") as g:
                g.write(str(config))
                return True
    else:
        error(f"The config file does not exist", "red")
        exit()

def get_config(filename, subreddit):
    if os.path.exists(filename):
        bn = os.path.basename(filename)
        if bn == "self_post.yml":
            with open(filename, "r") as f:
                cache = load(f, Loader=FullLoader)

                for i in cache:
                    if i["subreddit"] == subreddit:
                        return i
                    else:
                        return None
        
        elif bn in ["post.yml", "comment.yml"]:
            pass

    else:
        error(f"The requested cache file does not exist.", "red")
        exit()

def set_cache(filename, subreddit, timestamp, **kwargs):
    if os.path.exists(filename):
        bn = os.path.basename(filename)

        if bn == "self_post.yml":
            with open(filename, "r") as f:
                cache = load(f, Loader=FullLoader)
                cache_object = {"subreddit": subreddit.display_name,"timestamp": str(timestamp)}

                for i in range(len(cache)):
                    if cache[i]['subreddit'] == subreddit:
                        cache.pop(i)

                cache += [cache_object]
                
                with open(filename, "w") as g:
                    g.write(str(cache))

        elif bn in ["post.yml", "comment.yml"]:
            content = kwargs.get("content_id")
            
            with open(filename, "r") as f:
                cache = load(f, Loader=FullLoader)
                cache += [content]

                with open(filename, "w") as f:
                    f.write(str(cache))
    else:
        error(f"The requested cache file does not exist.", "red")
        exit()

def get_cache(filename, **kwargs):
    bn = os.path.basename(filename)

    if bn == "self_post.yml":
        with open(filename, "r") as f:
            cache = load(f, Loader=FullLoader)
            sub = kwargs.get("subreddit")
            content_exists = False
           
            for i in cache:
                if i['subreddit'] == sub:
                    content_exists = True
                    return i

            return False

    elif bn in ["post.yml", "comment.yml"]:
        with open(filename, "r") as f:
            cache = load(f, Loader=FullLoader)
            content_id = kwargs.get("content_id")
            
            if content_id in cache:
                return True
            else:
                return False

def flush_cache(flush_config):
    cache_files = [ "cache/comment.yml", "cache/post.yml", "cache/self_post.yml"]

    for cf in cache_files:
        with open(cf, "w") as f:
            f.write("[]")

    if flush_config == True:
        with open("cache/config.yml", "w") as config:
            config.write("{}")
