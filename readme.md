# Reddit-Brain

```
       __
   _  |@@|
  / \ \--/ __
  ) O|----|  |   __
 / / \ }{ /\ )_ / _|
 )/  /\__/\ \__O (__
|/  (--/\--)    \__/
/   _)(  )(_
   `---''---`
```

**karma farming bot for mere mortals in PRAW + Click.** use on a fresh account, set, forget, and watch redditors stare in envy at your fake internet points. it takes into account all types of bans/ratelimits/etc you can get, and is designed/frequently updated to avoid them in the most efficent manner possible. :)

yes, it does in fact also farm comment karma. Be careful when using this since reddit pays power mods one-by-one to sit around all day and nip any bot accounts in the bud despite any reason or false flags. just don't use this on your main account

## How to Basic
1. clone the repo: ```git clone --depth=1 https://gitlab.com/s1nistr4/reddit-brain.git```
2. create a virtual environment ```python -m venv env; source env/bin/activate; pip3 install --upgrade pip setuptools```
3. install this crap ```pip3 install -r requirements.txt```

you can run it with ```py rf.py -i <client_id> -c <client_secret> -u <username> -p <password>```
yeah, icup, haahahahahh

pass in a ```client_id```, ```client_secret```, ```username``` and ```password```. i recommend you make the command and save it as a shell alias or some crap
it's up to you to find out how to get those ids, if you can't [goolag](http://www.goolag.com/index.html) search then that's your own problem.

## notice
this doesn't work well on fresh accounts. for now, use it on accounts with at least 100 post karma.

<hr>
(C) s1nistr4
